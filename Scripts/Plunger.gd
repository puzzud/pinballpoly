extends Spatial

var ball = null

export(float) var activeForce = 1.0

func _ready():
	pass

func _physics_process(delta):
	if !Global.activeGame:
		return
	
	if ball == null:
		return
	
	if Input.is_action_just_pressed("ui_select"):
		shootBall()

func onBodyEntered(body):
	ball = body

func onBodyExited(body):
	ball = null

func shootBall():
	if ball == null:
		return
	
	var bounceImpulse = Vector3.FORWARD * activeForce
	
	var ballLinearVelocity = ball.linear_velocity + bounceImpulse
	ball.linear_velocity = ballLinearVelocity
	
	$AudioPlayers/Bump.play()
