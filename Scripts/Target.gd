extends Spatial

export(bool) var closed = true

export(bool) var activateStateIsClosed = false

export(bool) var touchActivatable = true

export(float) var activationMoney = 0.25

onready var initialPosition = transform.origin

func _ready():
	setClosed(closed)

func _physics_process(delta):
	pass

func onBodyEntered(body):
	if touchActivatable:
		onInputActivated()

func onInputActivated():
	if activateStateIsClosed && closed:
		return
		
	activate()

func activate():
	if !Global.activeGame:
		return
	
	setClosed(activateStateIsClosed)
	
	if activationMoney > 0.0:
		Global.game.giveCurrentPlayerMoney(activationMoney)
	
	$AudioPlayers/Activate.play()

func onTweensCompleted():
	closed = !closed

func open():
	pass

func close():
	pass

func setClosed(closed):
	self.closed = closed
	
	if closed:
		transform.origin = getClosedPosition()
	else:
		transform.origin = getOpenPosition()
	
	$Body/CollisionShape.disabled = !closed
	$Area/CollisionShape.disabled = !closed

func getOpenPosition():
	var closedPosition = initialPosition
	closedPosition.y = closedPosition.y - $CSGBox.height + 0.01
	
	return closedPosition

func getClosedPosition():
	return initialPosition
