extends Spatial

export(float) var bounceForce = 45.0

export(float) var activationMoney = 0.25

func _ready():
	pass

func onBodyEntered(body):
	if !Global.activeGame:
		return
	
	var bodyPosition = body.get_global_transform().origin
	var position = get_global_transform().origin
	position.y = bodyPosition.y
	var bounceImpulse = position.direction_to(bodyPosition) * bounceForce
	
	var bodyLinearVelocity = body.linear_velocity + bounceImpulse
	body.linear_velocity = bodyLinearVelocity
	
	Global.game.giveCurrentPlayerMoney(activationMoney)
	
	$AudioPlayers/Activate.play()
