extends Spatial

export(Material) var inactiveGelMaterial
export(Array, Material) var gelMaterials

export(int) var activeGelIndex = 0

var activated = false

func _ready():
	Global.floorLights[activeGelIndex] = self
	
	if Global.propertyOwnerIndex[activeGelIndex] > -1:
		setActivated(true)

func setActivated(activated):
	self.activated = activated
	
	if activated:
		$Gel.material = gelMaterials[activeGelIndex]
	else:
		$Gel.material = inactiveGelMaterial

func onInputActivated():
	setActivated(true)

func processPropertyOwnership():
	Global.processPropertyOwnership(Global.currentPlayerIndex, activeGelIndex)
	
	return Global.propertyOwnerIndex[activeGelIndex] > -1
