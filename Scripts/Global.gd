extends Node

var game = null

var screenIndex = 0
var activeGame = false

var currentPlayerIndex = 0

var activeBalls = []

var ballLossCost = 20.0

var startingPlayerMoney = [ballLossCost, ballLossCost]
var playerMoney = [0.0, 0.0]

var propertyValue = [80.0, 100.0, 120.0, 140.0, 160.0, 180.0, 200.0, 250.0]
var propertyRentValue = [5.0, 10.0, 15.0, 20.0, 25.0, 30.0, 40.0, 50.0]
var propertyOwnerIndex = [-1, -1, -1, -1, -1, -1, -1, -1]

var startingPropertyOwnerIndex = [-1, -1, -1, -1, -1, -1, -1, -1]

var floorLights = [null, null, null, null, null, null, null, null]

func _ready():
	pass

func resetGameState():
	playerMoney = startingPlayerMoney.duplicate()
	propertyOwnerIndex = startingPropertyOwnerIndex.duplicate()

func registerBall(ball):
	activeBalls.append(ball)

func removeBall(ball):
	var index = activeBalls.find(ball)
	if index < 0:
		return
	
	activeBalls.remove(index)
	
	ball.queue_free()
	
	if activeBalls.empty():
		Global.game.takePlayerMoney(currentPlayerIndex, ballLossCost)
		
		if Global.activeGame:
			currentPlayerIndex = currentPlayerIndex + 1
			if currentPlayerIndex > 1:
				currentPlayerIndex = 0
				
			get_tree().reload_current_scene()

func processPropertyOwnership(playerIndex, propertyIndex):
	# Check if a player already owns this property.
	var propertyOwnerIndex = Global.propertyOwnerIndex[propertyIndex]
	if propertyOwnerIndex < 0:
		# Check if player can buy this property.
		var playerMoney = Global.playerMoney[playerIndex]
		var propertyValue = Global.propertyValue[propertyIndex]
		if playerMoney >= propertyValue:
			Global.playerPurchaseProperty(playerIndex, propertyIndex)
			
			Global.game.get_node("AudioPlayers/Buy").play()
	elif propertyOwnerIndex != playerIndex:
		# Make this player pay rent.
		Global.playerPayPropertyRent(playerIndex, propertyIndex)
		
		Global.game.get_node("AudioPlayers/Pay").play()

func playerPurchaseProperty(playerIndex, propertyIndex):
	Global.game.takePlayerMoney(playerIndex, propertyValue[propertyIndex])
	Global.propertyOwnerIndex[propertyIndex] = playerIndex
	
	game.updatePlayersProperties()

func playerPayPropertyRent(playerIndex, propertyIndex):
	var ownerIndex = propertyOwnerIndex[propertyIndex]
	
	var rentCost = propertyRentValue[propertyIndex]
	
	Global.game.takePlayerMoney(playerIndex, rentCost)
	Global.game.givePlayerMoney(ownerIndex, rentCost)
