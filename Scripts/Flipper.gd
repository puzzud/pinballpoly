extends Spatial

export(bool) var reverseDirection = false

export(float) var activationSpeed = 45.0

var justMaxed = false

func _ready():
	pass

func _physics_process(delta):
	var rotationDegrees = $Paddle.get_rotation_degrees()

	var activateLeft = false
	var activateRight = false
	
	if Global.activeGame:
		activateLeft = Input.is_action_pressed("ui_left")
		activateRight = Input.is_action_pressed("ui_right")

	if !reverseDirection:
		if activateLeft:
			if rotationDegrees.y < 15.0:
				$Paddle.angular_velocity.y = activationSpeed
			else:
				$Paddle.angular_velocity.y = 0.0
				
				if !justMaxed:
					justMaxed = true
					$AudioPlayers/JustMaxed.play()
		else:
			rotationDegrees.y = 0.0
			$Paddle.set_rotation_degrees(rotationDegrees)
			
			justMaxed = false
	else:
		if activateRight:
			if rotationDegrees.y > -15.0:
				$Paddle.angular_velocity.y = -activationSpeed
			else:
				$Paddle.angular_velocity.y = 0.0
				
				if !justMaxed:
					justMaxed = true
					$AudioPlayers/JustMaxed.play()
		else:
			rotationDegrees.y = 0.0
			$Paddle.set_rotation_degrees(rotationDegrees)
			
			justMaxed = false
