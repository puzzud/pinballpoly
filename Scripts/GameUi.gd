extends CanvasLayer

func _ready():
	pass

func makeTitleUiActive():
	$Title.visible = true
	$Game.visible = false

func makeGameUiActive():
	$Title.visible = false
	$Game.visible = true
	
	$Game/Controls.visible = true
	$Timers/ControlsLabelTimer.start()

func updateCurrentPlayer():
	for i in range(0, 2):
		var playerStats = getPlayerStats(i)
		var playerLabel = playerStats.get_node("VBoxContainer/Player Label")
		
		if Global.currentPlayerIndex == i:
			playerLabel["custom_colors/font_color"] = Color("f7df7b")
		else:
			playerLabel["custom_colors/font_color"] = Color("ffffff")

func updatePlayerMoney(playerId, moneyAmount):
	var playerStats = getPlayerStats(playerId)
	
	var playerMoneyValueLabel = playerStats.get_node("VBoxContainer/Money Value")
	
	var moneyString = getMoneyString(moneyAmount)
	playerMoneyValueLabel.set_text(moneyString)

func getPlayerStats(playerId):
	if playerId == 0:
		return $"Game/Player 1 Stats"
	elif playerId == 1:
		return $"Game/Player 2 Stats"
	else:
		return null

func getMoneyString(moneyAmount):
	var integerAmount = int(moneyAmount)
	var subDecimalAmount = int(round((float(moneyAmount) - integerAmount) * 100.0))
	
	var subDecimalAmountString = str(subDecimalAmount)
	while len(subDecimalAmountString) < 2:
		subDecimalAmountString = subDecimalAmountString + "0"
	
	return "$" + str(integerAmount) + "." + subDecimalAmountString

func updatePlayersProperties():
	updatePlayerProperties(0)
	updatePlayerProperties(1)
	
func updatePlayerProperties(playerIndex):
	var properties = getPlayerStats(playerIndex).get_node("VBoxContainer/Properties")
	
	for i in range(0, 8):
		var label = properties.get_node(str(i))
		
		if Global.propertyOwnerIndex[i] == playerIndex:
			label["custom_colors/font_color"] = Global.floorLights[i].gelMaterials[i].albedo_color
			label.visible = true
		else:
			label.visible = false

func showWinningPlayer(winningPlayerNumber):
	var screenCaption = $"Game/Screen Caption"
	
	screenCaption.text = "Player " + str(winningPlayerNumber) + " Wins!"
	screenCaption.visible = true

func onControlsLabelTimerTimeout():
	$Game/Controls.visible = false
