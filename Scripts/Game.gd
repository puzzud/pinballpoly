extends Spatial

var ui = null

func _ready():
	Global.game = self
	
	ui = $UI
	
	if Global.screenIndex == 0:
		#$"Cameras/Perspective Camera".current = false
		#$"Cameras/Backboard Camera".current = false#true
		$"Cameras/Camera".transform = $"Cameras/Backboard Camera".transform
		#$Cameras/Tween.interpolate_property($"Cameras/Camera", "transform", $"Cameras/Camera".transform, $"Cameras/Backboard Camera".transform, 1.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
		#$Cameras/Tween.start()
		
		ui.makeTitleUiActive()
	else:
		#$"Cameras/Backboard Camera".current = false
		#$"Cameras/Perspective Camera".current = false#true
		$"Cameras/Camera".transform = $"Cameras/Perspective Camera".transform
		#$Cameras/Tween.interpolate_property($"Cameras/Camera", "transform", $"Cameras/Camera".transform, $"Cameras/Perspective Camera".transform, 1.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
		#$Cameras/Tween.start()
		
		ui.makeGameUiActive()
	
	if Global.activeGame:
		updateCurrentPlayer()
		updatePlayersMoney()
		updatePlayersProperties()
		updateFloorLights()

func _process(delta):
	if Global.screenIndex == 0:
		if Input.is_key_pressed(KEY_SPACE):
			Global.activeGame = true
			Global.screenIndex = 1
			
			Global.resetGameState()
			
			#$"Cameras/Backboard Camera".current = false
			#$"Cameras/Perspective Camera".current = false#true
			#$"Cameras/Camera".transform = $"Cameras/Perspective Camera".transform
			$Cameras/Tween.interpolate_property($"Cameras/Camera", "transform", $"Cameras/Camera".transform, $"Cameras/Perspective Camera".transform, 1.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
			$Cameras/Tween.start()
			
			ui.makeGameUiActive()
			
			updateCurrentPlayer()
			updatePlayersMoney()
			updatePlayersProperties()
			updateFloorLights()

func updateCurrentPlayer():
	ui.updateCurrentPlayer()

func updatePlayersMoney():
	updatePlayerMoney(0)
	updatePlayerMoney(1)

func updatePlayerMoney(playerId):
	ui.updatePlayerMoney(playerId, Global.playerMoney[playerId])

func giveCurrentPlayerMoney(moneyAmount):
	givePlayerMoney(Global.currentPlayerIndex, moneyAmount)

func givePlayerMoney(playerIndex, moneyAmount):
	Global.playerMoney[playerIndex] = Global.playerMoney[playerIndex] + moneyAmount
	
	updatePlayerMoney(playerIndex)

func takePlayerMoney(playerIndex, moneyAmount):
	if Global.playerMoney[playerIndex] < moneyAmount:
		if sellPlayerPropertiesToPay(playerIndex, moneyAmount) > 0:
			updatePlayersProperties()
			updateFloorLights()
	
	Global.playerMoney[playerIndex] = Global.playerMoney[playerIndex] - moneyAmount
	
	var isGameOver = false
	
	if Global.playerMoney[playerIndex] <= 0.0:
		#Global.playerMoney[playerIndex] = 0.0
		isGameOver = true
	
	updatePlayerMoney(playerIndex)
	
	if isGameOver:
		endGame()

func sellPlayerPropertiesToPay(playerIndex, moneyAmount):
	var numberOfPropertiesSold = 0
	
	while Global.playerMoney[playerIndex] < moneyAmount:
		var cheapestPropertyIndex = getCheapeastPropertyOwnedBy(playerIndex)
		if cheapestPropertyIndex == -1:
			break
		
		sellPlayerProperty(playerIndex, cheapestPropertyIndex)
		
		numberOfPropertiesSold = numberOfPropertiesSold + 1
	
	return numberOfPropertiesSold

func sellPlayerProperty(playerIndex, propertyIndex):
	Global.playerMoney[playerIndex] = Global.playerMoney[playerIndex] + Global.propertyValue[propertyIndex]
	Global.propertyOwnerIndex[propertyIndex] = -1

func getCheapeastPropertyOwnedBy(playerIndex):
	for i in range(0, 8):
		if Global.propertyOwnerIndex[i] == playerIndex:
			return i
	
	return -1

func updatePlayersProperties():
	ui.updatePlayersProperties()

func updateFloorLights():
	for i in range(0, Global.floorLights.size()):
		Global.floorLights[i].setActivated(Global.propertyOwnerIndex[i] > -1)

func endGame():
	Global.activeGame = false
	
	var winningPlayerIndex = -1
	if Global.playerMoney[0] <= 0.0:
		winningPlayerIndex = 1
	else:
		winningPlayerIndex = 0
	
	var winningPlayerNumber = winningPlayerIndex + 1
	
	ui.showWinningPlayer(winningPlayerNumber)
	
	$Timers/GameOverTimer.start()
	
	$AudioPlayers/Win.play()

func onGameOverTimerTimeout():
	Global.screenIndex = 0
	get_tree().reload_current_scene()
