extends Spatial

signal activated
signal deactivated

const floorLightClass = preload("res://Scripts/FloorLight.gd")

var activated = false

func _ready():
	pass

func activate():
	activated = true
	
	emit_signal("activated")

func deactivate():
	activated = false
	
	emit_signal("deactivated")

func onBodyEntered(body):
	var parent = get_parent()
	if parent is floorLightClass:
		if !parent.processPropertyOwnership():
			return
	else:
		if activated:
			return
	
	activate()
